package com.trossky.core.auth.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class AuthControllerTest {
  @Autowired
  AuthController authController;
  String msn;

  @BeforeEach
  void setUp() {
    msn = "Health OK!";
  }

  @Test
  void validMessaje() {
    String result = authController.healthOK();
    assertEquals(msn, result);
  }


}
