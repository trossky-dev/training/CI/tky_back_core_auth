# Backend CORE AUTH

This project is a example of backend core type, for this example the word AUTH is symbolic to the activity of CI.

## Endpoint

```scheme
URI: /

Response: Health OK!
```

©[Luis Fernando García](trossky.com)